﻿using AutoMapper;
using IMDB.Api.ViewModels;
using IMDB.Domain.Entities;

namespace IMDB.Api.AutoMapper
{
    public class AutoMapperSetup : Profile
    {
        public AutoMapperSetup()
        {
            CreateMap<LoginViewModel, Usuario>().ReverseMap();            
        }
    }
}
