﻿using IMDB.Api.Services;
using IMDB.Api.ViewModels;
using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Api.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public UsuariosController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public ActionResult<dynamic> Login([FromBody] LoginViewModel login)
        {
            if (string.IsNullOrEmpty(login.Login) || string.IsNullOrEmpty(login.Senha))
                return BadRequest();

            var usuario = _usuarioService.Login(login.Login, login.Senha);
            if (usuario == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            var token = TokenService.GerarToken(usuario);
            usuario.Senha = "";

            return new
            {
                usuario,
                token
            };
        }

        [HttpGet]
        [Authorize(Roles = "visitante, administrador")]
        public IEnumerable<Usuario> Get()
        {
            return _usuarioService.ObterAtivos();
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "visitante, administrador")]
        public Usuario Get(Guid id)
        {
            return _usuarioService.ObterPorId(id);
        }

        [HttpPost]
        [Authorize(Roles = "visitante, administrador")]
        public Usuario Post([FromBody] Usuario usuario)
        {
            return _usuarioService.Adicionar(usuario);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "visitante, administrador")]
        public void Put(Guid id, [FromBody] Usuario usuario)
        {
            if (id != usuario.Id)
            {
                BadRequest();
            }

            _usuarioService.Atualizar(usuario);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "administrador")]
        public void Delete(Guid id)
        {
            var usuario = _usuarioService.ObterPorId(id);

            if (usuario == null)
                NotFound(new { message = "Usuário não encontrado" });

            _usuarioService.Remover(usuario);
        }
    }
}
