﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Api.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly IFilmeService _filmeService;

        public FilmesController(IFilmeService filmeService)
        {
            _filmeService = filmeService;
        }

        [HttpGet]
        [Authorize(Roles = "visitante, administrador")]
        public IEnumerable<Filme> Get(int? pagina, int? quantidade)
        {
            return _filmeService.ObterPorOrdemAlfabetica(pagina, quantidade);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "visitante, administrador")]
        public Filme Get(Guid id)
        {
            return _filmeService.ObterPorId(id);
        }

        [HttpPost]
        [Authorize(Roles = "administrador")]
        public Filme Post(Filme filme)
        {
            return _filmeService.Adicionar(filme);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "visitante, administrador")]
        public void Put(Guid id, Filme filme)
        {
            if (id != filme.Id)
            {
                BadRequest();
            }

            _filmeService.Atualizar(filme);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "visitante, administrador")]
        public void Delete(Guid id)
        {
            var filme = _filmeService.ObterPorId(id);

            if (filme == null)
                NotFound(new { message = "Filme não encontrado" });

            _filmeService.Remover(filme);
        }
    }
}
