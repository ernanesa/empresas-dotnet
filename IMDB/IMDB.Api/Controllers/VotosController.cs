﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMDB.Api.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class VotosController : ControllerBase
    {
        private readonly IVotoService _votoService;

        public VotosController(IVotoService votoService)
        {
            _votoService = votoService;
        }

        [HttpPost]
        [Authorize(Roles = "visitante")]
        public Voto Post([FromBody] Voto voto)
        {
            return _votoService.Adicionar(voto);
        }
    }
}
