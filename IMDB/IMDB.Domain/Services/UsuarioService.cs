﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace IMDB.Domain.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public Usuario Adicionar(Usuario usuario)
        {
            return _usuarioRepository.Adicionar(usuario);
        }

        public void Atualizar(Usuario usuario)
        {
            _usuarioRepository.Atualizar(usuario);
        }

        public Usuario ObterPorId(Guid id)
        {
            return _usuarioRepository.ObterPorId(id);
        }

        public IEnumerable<Usuario> ObterTodos()
        {
            return _usuarioRepository.ObterTodos();
        }

        public void Remover(Usuario usuario)
        {
            _usuarioRepository.Remover(usuario);
        }

        public Usuario Login(string login, string senha)
        {
            return _usuarioRepository.Login(login, senha);
        }

        public IEnumerable<Usuario> ObterAtivos()
        {
            return _usuarioRepository.ObterAtivos();
        }
    }
}
