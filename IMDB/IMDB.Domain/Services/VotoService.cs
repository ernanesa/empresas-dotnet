﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Domain.Interfaces.Services;

namespace IMDB.Domain.Services
{
    public class VotoService : IVotoService
    {
        private readonly IVotoRepository _votoRepository;

        public VotoService(IVotoRepository votoRepository)
        {
            _votoRepository = votoRepository;
        }

        public Voto Adicionar(Voto voto)
        {
            return _votoRepository.Adicionar(voto);
        }
    }
}
