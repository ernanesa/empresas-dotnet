﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace IMDB.Domain.Services
{
    public class FilmeService : IFilmeService
    {
        private readonly IFilmeRepository _filmeRepository;

        public FilmeService(IFilmeRepository filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        public Filme Adicionar(Filme filme)
        {
            return _filmeRepository.Adicionar(filme);
        }

        public void Atualizar(Filme filme)
        {
            _filmeRepository.Atualizar(filme);
        }

        public IEnumerable<Filme> ObterPorOrdemAlfabetica(int? pagina, int? quantidade)
        {
            return _filmeRepository.ObterPorOrdemAlfabetica(pagina, quantidade);
        }

        public Filme ObterPorId(Guid id)
        {
            return _filmeRepository.ObterPorId(id);
        }

        public IEnumerable<Filme> ObterTodos()
        {
            return _filmeRepository.ObterTodos();   
        }

        public void Remover(Filme filme)
        {
            _filmeRepository.Remover(filme);
        }
    }
}
