﻿using IMDB.Domain.Entities;
using System;
using System.Collections.Generic;

namespace IMDB.Domain.Interfaces.Repository
{
    public interface IFilmeRepository : IRepository<Filme>
    {
        IEnumerable<Filme> ObterPorOrdemAlfabetica(int? pagina, int? quantidade);
        Filme ObterPorId(Guid id);
        void Remover(Filme filme);
    }
}
