﻿using IMDB.Domain.Entities;
using System;
using System.Collections.Generic;

namespace IMDB.Domain.Interfaces.Repository
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        void Remover(Usuario usuario);
        Usuario Login(string login, string senha);
        Usuario ObterPorId(Guid id);
        IEnumerable<Usuario> ObterAtivos();
    }
}
