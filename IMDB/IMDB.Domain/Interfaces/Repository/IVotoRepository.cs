﻿using IMDB.Domain.Entities;

namespace IMDB.Domain.Interfaces.Repository
{
    public interface IVotoRepository
    {
        Voto Adicionar(Voto voto);
    }
}
