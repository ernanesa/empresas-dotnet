﻿using IMDB.Domain.Entities;
using System.Collections.Generic;

namespace IMDB.Domain.Interfaces.Services
{
    public interface IUsuarioService : IService<Usuario>
    {
        Usuario Login(string login, string senha);
        IEnumerable<Usuario> ObterAtivos();
    }
}
