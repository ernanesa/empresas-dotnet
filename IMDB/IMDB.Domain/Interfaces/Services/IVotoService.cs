﻿using IMDB.Domain.Entities;

namespace IMDB.Domain.Interfaces.Services
{
    public interface IVotoService
    {
        Voto Adicionar(Voto voto);
    }
}
