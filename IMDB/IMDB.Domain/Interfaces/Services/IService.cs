﻿using System;
using System.Collections.Generic;

namespace IMDB.Domain.Interfaces.Services
{
    public interface IService<TEntity> where TEntity : class
    {
        TEntity Adicionar(TEntity entity);
        void Atualizar(TEntity entity);
        TEntity ObterPorId(Guid id);
        IEnumerable<TEntity> ObterTodos();
        void Remover(TEntity entity);
    }
}