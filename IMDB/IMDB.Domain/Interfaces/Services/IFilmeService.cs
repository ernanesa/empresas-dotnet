﻿using IMDB.Domain.Entities;
using System.Collections.Generic;

namespace IMDB.Domain.Interfaces.Services
{
    public interface IFilmeService : IService<Filme>
    {
        IEnumerable<Filme> ObterPorOrdemAlfabetica(int? pagina, int? quantidade);
    }
}
