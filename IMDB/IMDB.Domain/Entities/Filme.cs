﻿using System;
using System.Collections.Generic;

namespace IMDB.Domain.Entities
{
    public class Filme
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Diretor { get; set; }
        public string Genero { get; set; }
        public List<Ator> Atores { get; set; }
    }
}
