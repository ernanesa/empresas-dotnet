﻿using System;

namespace IMDB.Domain.Entities
{
    public class Ator
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public Guid FilmeId { get; set; }
    }
}
