﻿using System;

namespace IMDB.Domain.Entities
{
    public class Voto
    {
        public Guid Id { get; set; }
        public Guid UsuarioId { get; set; }
        public Guid FilmeId { get; set; }
        public int Pontuacao { get; set; }
    }
}
