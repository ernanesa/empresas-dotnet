﻿using IMDB.Domain.Interfaces.Repository;
using IMDB.Domain.Interfaces.Services;
using IMDB.Domain.Services;
using IMDB.Infrastructure.Data;
using IMDB.Infrastructure.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace IMDB.Infrastructure.IoC
{
    public class NativeInjector
    {
        public static void RegisterServices(IServiceCollection service)
        {
            service.AddScoped<IMDBContext>();

            service.AddScoped<IFilmeService, FilmeService>();
            service.AddScoped<IUsuarioService, UsuarioService>();
            service.AddScoped<IVotoService, VotoService>();


            service.AddScoped<IFilmeRepository, FilmeRepository>();
            service.AddScoped<IUsuarioRepository, UsuarioRepository>();
            service.AddScoped<IVotoRepository, VotoRepository>();
        }
    }
}
