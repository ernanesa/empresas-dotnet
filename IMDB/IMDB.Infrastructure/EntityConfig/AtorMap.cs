﻿using IMDB.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDB.Infrastructure.EntityConfig
{
    public class AtorMap : IEntityTypeConfiguration<Ator>
    {
        public void Configure(EntityTypeBuilder<Ator> builder)
        {
            builder.ToTable("Ator");

            builder.HasKey(ator => ator.Id);
            builder.Property(ator => ator.Nome)
                .HasColumnType("varchar(100)")
                .IsRequired();            
        }
    }
}