﻿using IMDB.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDB.Infrastructure.EntityConfig
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");

            builder.HasKey(usuario => usuario.Id);
            builder.Property(usuario => usuario.Nome)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(usuario => usuario.Login)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(usuario => usuario.Senha)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(usuario => usuario.Role)
                .HasColumnType("varchar(100)")
                .IsRequired();
        }
    }
}