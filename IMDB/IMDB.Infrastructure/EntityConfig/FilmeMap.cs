﻿using IMDB.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDB.Infrastructure.EntityConfig
{
    public class FilmeMap : IEntityTypeConfiguration<Filme>
    {
        public void Configure(EntityTypeBuilder<Filme> builder)
        {
            builder.ToTable("Filme");

            builder.HasKey(filme => filme.Id);
            builder.Property(filme => filme.Nome)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(filme => filme.Diretor)
                .HasColumnType("varchar(100)")
                .IsRequired();
            builder.Property(filme => filme.Genero)
                .HasColumnType("varchar(100)")
                .IsRequired();
        }
    }
}