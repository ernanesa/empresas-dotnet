﻿using IMDB.Domain.Entities;
using IMDB.Infrastructure.EntityConfig;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace IMDB.Infrastructure.Data
{
    public class IMDBContext : DbContext
    {
        public IMDBContext(DbContextOptions<IMDBContext> options) : base(options)
        {

        }

        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ator> Atores { get; set; }
        public DbSet<Voto> Votos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AtorMap());
            modelBuilder.ApplyConfiguration(new FilmeMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new VotoMap());


            #region Seeding
            modelBuilder.Entity<Usuario>().HasData(
                new Usuario
                {
                    Id = new System.Guid("41066648-17c1-4e6f-b870-db97c2b83055"),
                    Nome = "Administrador do sistema",
                    Login = "Administrador",
                    Senha = "administrador",
                    Role = "administrador",
                    Ativo = true
                },
                new Usuario
                {
                    Id = new System.Guid("d406c089-a808-40df-8003-69bed9f37fbb"),
                    Nome = "Usuário visitante",
                    Login = "Visitante",
                    Senha = "visitante",
                    Role = "visitante",
                    Ativo = true
                }
           );

            modelBuilder.Entity<Filme>().HasData(
                new Filme
                {
                    Id = new System.Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"),
                    Nome = "Um Sonho de Liberdade",
                    Diretor = "Frank Darabont",
                    Genero = "Drama"
                }, new Filme
                {
                    Id = new System.Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"),
                    Nome = "O Poderoso Chefão",
                    Diretor = "Francis Ford Coppola",
                    Genero = "Drama"
                }, new Filme
                {
                    Id = new System.Guid("29aec904-25d6-4630-b8e8-d87fd4174977"),
                    Nome = "Batman: O Cavaleiro das Trevas",
                    Diretor = "Christopher Nolan",
                    Genero = "Ação"
                });

            modelBuilder.Entity<Ator>().HasData(
                new Ator
                {
                    Id = new System.Guid("e9a0d909-b054-487d-9854-481077ebd27b"),
                    Nome = "Tim Robbins",
                    FilmeId = new System.Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4")
                }, new Ator
                {
                    Id = new System.Guid("b40ae9bb-670e-4e5a-a3c8-b8be9356e9c3"),
                    Nome = "Morgan Freeman",
                    FilmeId = new System.Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4")
                }, new Ator
                {
                    Id = new System.Guid("510e4e39-1243-48dd-9aac-38375b755f5f"),
                    Nome = "Bob Gunton",
                    FilmeId = new System.Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4")
                }, new Ator
                {
                    Id = new System.Guid("273e77ab-1744-4195-9eaf-212b852311e0"),
                    Nome = "Marlon Brando",
                    FilmeId = new System.Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f")
                }, new Ator
                {
                    Id = new System.Guid("684e2f92-b960-4476-b667-b93753b741f6"),
                    Nome = "Al Pacino",
                    FilmeId = new System.Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f")
                }, new Ator
                {
                    Id = new System.Guid("80ab5062-6bf2-4ef9-8729-01f0307af592"),
                    Nome = "Christian Bale",
                    FilmeId = new System.Guid("29aec904-25d6-4630-b8e8-d87fd4174977")
                }
           );
            #endregion
        }
    }
}
