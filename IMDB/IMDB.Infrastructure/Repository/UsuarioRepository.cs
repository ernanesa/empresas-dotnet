﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Infrastructure.Repository
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(IMDBContext dbContext) : base(dbContext)
        {
        }

        public Usuario Login(string login, string senha)
        {
            return _dbContext.Set<Usuario>().Where(u => u.Login.ToLower() == login.ToLower() && u.Senha == senha && u.Ativo).FirstOrDefault();
        }

        public IEnumerable<Usuario> ObterAtivos()
        {
            return _dbContext.Set<Usuario>().Where(x => x.Role != "administrador" && x.Ativo).OrderBy(x => x.Nome).AsEnumerable();
        }

        public Usuario ObterPorId(Guid id)
        {
            return _dbContext.Set<Usuario>().Find(id);
        }

        public void Remover(Usuario usuario)
        {
            usuario.Ativo = false;
            _dbContext.Entry(usuario).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }
    }
}
