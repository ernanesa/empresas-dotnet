﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Infrastructure.Data;

namespace IMDB.Infrastructure.Repository
{
    public class VotoRepository : IVotoRepository
    {
        protected readonly IMDBContext _dbContext;

        public VotoRepository(IMDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Voto Adicionar(Voto voto)
        {
            _dbContext.Set<Voto>().Add(voto);
            _dbContext.SaveChanges();
            return voto;
        }
    }
}
