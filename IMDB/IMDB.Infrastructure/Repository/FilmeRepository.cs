﻿using IMDB.Domain.Entities;
using IMDB.Domain.Interfaces.Repository;
using IMDB.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Infrastructure.Repository
{
    public class FilmeRepository : Repository<Filme>, IFilmeRepository
    {
        public FilmeRepository(IMDBContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Filme> ObterPorOrdemAlfabetica(int? pagina, int? quantidade)
        {
            pagina ??= 1;
            quantidade ??= 10;

            if (pagina <= 0) pagina = 1;

            var pular = (pagina - 1) * quantidade;

            return _dbContext.Set<Filme>().Include(filme => filme.Atores).OrderBy(x => x.Nome).Skip((int)pular).Take((int)quantidade).AsEnumerable();
        }

        public Filme ObterPorId(Guid id)
        {
            return _dbContext.Set<Filme>().Include(filme => filme.Atores).Where(x => x.Id == id).FirstOrDefault();
        }

        public void Remover(Filme filme)
        {
            _dbContext.Set<Filme>().Remove(filme);
            _dbContext.SaveChanges();
        }
    }
}
