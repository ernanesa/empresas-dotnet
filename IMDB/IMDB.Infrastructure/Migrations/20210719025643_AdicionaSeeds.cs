﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDB.Infrastructure.Migrations
{
    public partial class AdicionaSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Filme",
                columns: new[] { "Id", "Diretor", "Genero", "Nome" },
                values: new object[,]
                {
                    { new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"), "Frank Darabont", "Drama", "Um Sonho de Liberdade" },
                    { new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"), "Francis Ford Coppola", "Drama", "O Poderoso Chefão" },
                    { new Guid("29aec904-25d6-4630-b8e8-d87fd4174977"), "Christopher Nolan", "Ação", "Batman: O Cavaleiro das Trevas" }
                });

            migrationBuilder.InsertData(
                table: "Usuario",
                columns: new[] { "Id", "Ativo", "Login", "Nome", "Role", "Senha" },
                values: new object[,]
                {
                    { new Guid("41066648-17c1-4e6f-b870-db97c2b83055"), true, "Administrador", "Administrador do sistema", "administrador", "administrador" },
                    { new Guid("d406c089-a808-40df-8003-69bed9f37fbb"), true, "Visitante", "Usuário visitante", "visitante", "visitante" }
                });

            migrationBuilder.InsertData(
                table: "Ator",
                columns: new[] { "Id", "FilmeId", "Nome" },
                values: new object[,]
                {
                    { new Guid("e9a0d909-b054-487d-9854-481077ebd27b"), new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"), "Tim Robbins" },
                    { new Guid("b40ae9bb-670e-4e5a-a3c8-b8be9356e9c3"), new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"), "Morgan Freeman" },
                    { new Guid("510e4e39-1243-48dd-9aac-38375b755f5f"), new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"), "Bob Gunton" },
                    { new Guid("273e77ab-1744-4195-9eaf-212b852311e0"), new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"), "Marlon Brando" },
                    { new Guid("684e2f92-b960-4476-b667-b93753b741f6"), new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"), "Al Pacino" },
                    { new Guid("80ab5062-6bf2-4ef9-8729-01f0307af592"), new Guid("29aec904-25d6-4630-b8e8-d87fd4174977"), "Christian Bale" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("273e77ab-1744-4195-9eaf-212b852311e0"));

            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("510e4e39-1243-48dd-9aac-38375b755f5f"));

            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("684e2f92-b960-4476-b667-b93753b741f6"));

            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("80ab5062-6bf2-4ef9-8729-01f0307af592"));

            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("b40ae9bb-670e-4e5a-a3c8-b8be9356e9c3"));

            migrationBuilder.DeleteData(
                table: "Ator",
                keyColumn: "Id",
                keyValue: new Guid("e9a0d909-b054-487d-9854-481077ebd27b"));

            migrationBuilder.DeleteData(
                table: "Usuario",
                keyColumn: "Id",
                keyValue: new Guid("41066648-17c1-4e6f-b870-db97c2b83055"));

            migrationBuilder.DeleteData(
                table: "Usuario",
                keyColumn: "Id",
                keyValue: new Guid("d406c089-a808-40df-8003-69bed9f37fbb"));

            migrationBuilder.DeleteData(
                table: "Filme",
                keyColumn: "Id",
                keyValue: new Guid("29aec904-25d6-4630-b8e8-d87fd4174977"));

            migrationBuilder.DeleteData(
                table: "Filme",
                keyColumn: "Id",
                keyValue: new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"));

            migrationBuilder.DeleteData(
                table: "Filme",
                keyColumn: "Id",
                keyValue: new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"));
        }
    }
}
