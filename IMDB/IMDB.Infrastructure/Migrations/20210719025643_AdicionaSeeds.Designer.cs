﻿// <auto-generated />
using System;
using IMDB.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace IMDB.Infrastructure.Migrations
{
    [DbContext(typeof(IMDBContext))]
    [Migration("20210719025643_AdicionaSeeds")]
    partial class AdicionaSeeds
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.8")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("IMDB.Domain.Entities.Ator", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FilmeId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.HasIndex("FilmeId");

                    b.ToTable("Ator");

                    b.HasData(
                        new
                        {
                            Id = new Guid("e9a0d909-b054-487d-9854-481077ebd27b"),
                            FilmeId = new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"),
                            Nome = "Tim Robbins"
                        },
                        new
                        {
                            Id = new Guid("b40ae9bb-670e-4e5a-a3c8-b8be9356e9c3"),
                            FilmeId = new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"),
                            Nome = "Morgan Freeman"
                        },
                        new
                        {
                            Id = new Guid("510e4e39-1243-48dd-9aac-38375b755f5f"),
                            FilmeId = new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"),
                            Nome = "Bob Gunton"
                        },
                        new
                        {
                            Id = new Guid("273e77ab-1744-4195-9eaf-212b852311e0"),
                            FilmeId = new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"),
                            Nome = "Marlon Brando"
                        },
                        new
                        {
                            Id = new Guid("684e2f92-b960-4476-b667-b93753b741f6"),
                            FilmeId = new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"),
                            Nome = "Al Pacino"
                        },
                        new
                        {
                            Id = new Guid("80ab5062-6bf2-4ef9-8729-01f0307af592"),
                            FilmeId = new Guid("29aec904-25d6-4630-b8e8-d87fd4174977"),
                            Nome = "Christian Bale"
                        });
                });

            modelBuilder.Entity("IMDB.Domain.Entities.Filme", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Diretor")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Genero")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Filme");

                    b.HasData(
                        new
                        {
                            Id = new Guid("cf003f0c-617b-476e-957a-166bb7a7b3e4"),
                            Diretor = "Frank Darabont",
                            Genero = "Drama",
                            Nome = "Um Sonho de Liberdade"
                        },
                        new
                        {
                            Id = new Guid("856d1ea8-dcab-4d14-8720-1ce814a1353f"),
                            Diretor = "Francis Ford Coppola",
                            Genero = "Drama",
                            Nome = "O Poderoso Chefão"
                        },
                        new
                        {
                            Id = new Guid("29aec904-25d6-4630-b8e8-d87fd4174977"),
                            Diretor = "Christopher Nolan",
                            Genero = "Ação",
                            Nome = "Batman: O Cavaleiro das Trevas"
                        });
                });

            modelBuilder.Entity("IMDB.Domain.Entities.Usuario", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Role")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Senha")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Usuario");

                    b.HasData(
                        new
                        {
                            Id = new Guid("41066648-17c1-4e6f-b870-db97c2b83055"),
                            Ativo = true,
                            Login = "Administrador",
                            Nome = "Administrador do sistema",
                            Role = "administrador",
                            Senha = "administrador"
                        },
                        new
                        {
                            Id = new Guid("d406c089-a808-40df-8003-69bed9f37fbb"),
                            Ativo = true,
                            Login = "Visitante",
                            Nome = "Usuário visitante",
                            Role = "visitante",
                            Senha = "visitante"
                        });
                });

            modelBuilder.Entity("IMDB.Domain.Entities.Voto", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FilmeId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Pontuacao")
                        .HasColumnType("int");

                    b.Property<Guid>("UsuarioId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("Voto");
                });

            modelBuilder.Entity("IMDB.Domain.Entities.Ator", b =>
                {
                    b.HasOne("IMDB.Domain.Entities.Filme", null)
                        .WithMany("Atores")
                        .HasForeignKey("FilmeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("IMDB.Domain.Entities.Filme", b =>
                {
                    b.Navigation("Atores");
                });
#pragma warning restore 612, 618
        }
    }
}
